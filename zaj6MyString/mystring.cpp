//
// Created by Hubert on 31.05.2023.
//

#include <string>
#include "mystring.h"
#include <iostream>

size_t MyString::size() const {
    return current_size_;
}

size_t MyString::capacity() const {
    return this->text.capacity()+initialBufferSize_;
}

bool MyString::empty() const {
    return current_size_==0;
}

MyString::MyString(const char *words) {
    int size=0;
    const char *pointer=words;
    while (*pointer!='\0'){
        size+=1;
        pointer+=sizeof(char);
    }
    if (size<=20){
        for(int i=0;i<size;i++){
            buffer[i]=words[i];
        }
        current_size_=size;
    }
    else{
        for(int i=0;i<20;i++){
            buffer[i]=words[i];
        }
        text.assign(words+20);
        current_size_=initialBufferSize_+text.size();
    }
}

MyString::MyString() {

}

void MyString::clear() {
    current_size_=0;
    text="";
}

std::ostream &operator<<(std::ostream &os, const MyString &my_string) {
    char str[20];
    for(int i=0;i<20;i++){
        str[i]=my_string.buffer[i];
    }
    if (my_string.current_size_<=20){
        os.write(my_string.buffer,my_string.current_size_);
    }
    else{
        os.write(my_string.buffer,20);
        os<<my_string.text;
    }
    return os;
}

char MyString::operator[](size_t index) const {
    if (index < current_size_) {
        if (index < 20) {
            return buffer[index];
        } else {
            return text[index - 20];
        }
    } else {
        throw std::out_of_range("Index out of range");
    }
}

std::istream& operator>>(std::istream& is, MyString& myString) {
    char c;
    myString.text=" ";
    while (is.get(c)){
        if (myString.current_size_<=20){
            myString.buffer[myString.current_size_]=c;
            myString.current_size_+=1;
        }
        else{
            myString.text+=c;
            myString.current_size_=myString.initialBufferSize_+myString.text.size();
        }
    }
    return is;
}

void MyString::trim() {
    int i=0;
    int whiteCharsAtBegining=0;
    while ((*this)[i]==' ' ||(*this)[i]=='\t' || (*this)[i]=='\n'){
        i++;
        whiteCharsAtBegining++;
    }
    i=this->size()-1;
    int whiteCharsAtEnd=0;
    while ((*this)[i]==' ' ||(*this)[i]=='\t' || (*this)[i]=='\n'){
        i--;
        whiteCharsAtEnd++;
    }
    if(whiteCharsAtBegining<20){
        for(int i=0;i<20-whiteCharsAtBegining;i++){
            this->buffer[i]=this->buffer[i+whiteCharsAtBegining];
        }
        for(int i=whiteCharsAtBegining;i<20;i++){
            this->buffer[i]=this->text[i-whiteCharsAtBegining];
        }
        while (!text.empty() && std::isspace(text.back())) {
            text.pop_back();
        }
        this->text=text.substr(whiteCharsAtBegining);
        current_size_=initialBufferSize_+text.size();
    }
    else{
        while (!text.empty() && std::isspace(text.front())) {
            text.erase(0, 1);
        }
        while (!text.empty() && std::isspace(text.back())) {
            text.pop_back();
        }
        text.copy(buffer,20);
        this->text=text.substr(20);
        current_size_=initialBufferSize_+text.size();
    }
}

MyString &MyString::operator+=(const char character) {
    if (current_size_<20){
        this->buffer[current_size_]=character;
        current_size_++;
    }
    else{
        this->text+=character;
        current_size_++;
    }
    return *this;
}

char* MyString::begin() {
    return buffer;
}

const std::set<MyString> MyString::getUniqueWords() {
    return std::set<MyString>();
}

std::map<MyString, size_t> MyString::countWordsUsageIgnoringCases() const {
    return std::map<MyString, size_t>();
}

void MyString::toLower() {

}

MyString MyString::generateRandomWord(size_t) {
    return MyString();
}

bool MyString::startsWith(std::string word) const{
    return false;
}

bool MyString::endsWith(std::string word) const {
    return false;
}

bool MyString::operator==(const MyString &string) const {
    return false;
}

bool MyString::operator!=(const MyString &string) const {
    return false;
}

MyString MyString::join(const std::vector<MyString> &) const {
    return MyString();
}

bool MyString::all_of(int (*predicate)(int)) const {
    return false;
}

bool MyString::operator<(const MyString &string) const {
    return false;
}



