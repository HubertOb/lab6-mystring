//
// Created by Hubert on 31.05.2023.
//

#ifndef ZAD6MYSTRING_DLASTUDENTOW_MYSTRING_H
#define ZAD6MYSTRING_DLASTUDENTOW_MYSTRING_H


#include <cstddef>
#include <set>
#include <vector>
#include <algorithm>   // std::count_if
#include <type_traits> // std::is_same<>
#include <functional>  // std::bind2nd, std::not_equal_to
#include <cctype>      // isspace, isalpha, ...
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>

class MyString {
private:
    std::string text;
    size_t current_size_=0;
    char buffer[20];
public:
    static const size_t initialBufferSize_=20;

    MyString();
    MyString(const char words[]);
    size_t size() const;
    size_t capacity() const;
    bool empty() const;
    void clear();
    friend std::ostream& operator<<(std::ostream& os, const MyString &my_string);
    char operator[](size_t index) const;
    friend std::istream &operator>>(std::istream &is, MyString &myString);
    void trim();
    MyString& operator+=(const char character);
    char* begin();
    const std::set<MyString> getUniqueWords();
    std::map<MyString,size_t> countWordsUsageIgnoringCases() const;
    void toLower();
    static MyString generateRandomWord(size_t);
    bool startsWith(std::string word) const;
    bool endsWith(std::string word) const;
    bool operator==(const MyString &string) const;
    bool operator!=(const MyString &string) const;
    MyString join(const std::vector<MyString> &) const;
    bool all_of(int predicate(int)) const;
    bool operator<(const MyString &string) const;
};


#endif //ZAD6MYSTRING_DLASTUDENTOW_MYSTRING_H
