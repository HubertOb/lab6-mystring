Dear Student,

Your work hasn't been graded because of the problems listed below. Fix them and then push a commit (even empty will do) to notify me about the changes. Otherwise I won't be able to check your work... Contact your teacher in case you don't know what to do.

<details><summary>repo HubertOb/lab6-mystring is not private; repo HubertOb/lab6-mystring doesn&#x27;t include me as a maintainer.</summary></details>

-----------
I remain your faithful servant\
_Bobot_